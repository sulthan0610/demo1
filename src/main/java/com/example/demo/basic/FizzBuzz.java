package com.example.demo.basic;

public class FizzBuzz {
    public void FizzBuzz() {

        for (int i = 1; i <= 100; i++) {
            if (i % 3 == 0)
                System.out.println("FIZZ");
            else if (i % 5 == 0) {
                System.out.println("BUZZ");
            } else if (i % 3 == 0 && i % 5 == 0) {
                System.out.println("FizzBuzz");
            } else {
                System.out.println(i);
            }
        }
    }
}
