package com.example.demo.basic;

public class Luas {

   public Integer panjang;
   public Integer lebar;

    public Luas(){

    }

    public Luas(Integer panjang,Integer lebar){
        this.panjang = panjang;
        this.lebar = lebar;
    }

    public Integer hitungluas(){
        Integer luas = panjang * lebar;
        return luas;
    }


    public void setLebar(Integer lebar){
        this.lebar = lebar;
    }

    public void setPanjang(Integer panjang){
        this.panjang = panjang;
    }

    public Integer getLebar() {
        return lebar;
    }

    public Integer getPanjang(){
        return panjang;

    }
}
