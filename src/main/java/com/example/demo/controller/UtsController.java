package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/uts")
public class UtsController {
    @RequestMapping(value = "/uts",method = RequestMethod.GET)
    public String uts(){
        return "/uts/uts" ;
    }
}
