package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.management.modelmbean.ModelMBean;

@Controller
@RequestMapping("/bangunruang")
public class BangunRuangController {
    @RequestMapping(value = {"/",""} , method = RequestMethod.GET)
    public String index()
    {
        return "/bangunruang/index";
    }

    @RequestMapping(value = {"/segitiga"} , method = RequestMethod.GET)
    public String segitiga()
    {
        return "/bangunruang/segitiga";
    }

    @RequestMapping(value = {"/luas/{parameter}"} , method = RequestMethod.GET)
    public String luas(Model model, @PathVariable("parameter") String param1)
    {
        model.addAttribute("jenis",param1);

        return "/bangunruang/luas";
    }
}
